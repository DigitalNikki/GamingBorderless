﻿// File changed by DigitalNikki/Nicole M on Jan/21/2018
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GamingBorderless.Forms;
using GamingBorderless.Logic.Models;
using GamingBorderless.Logic.System;
using GamingBorderless.Logic.Windows;

namespace GamingBorderless
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Tools.Setup();
            //use github updating for non-steam
            if (Config.Instance.AppSettings.CheckForUpdates)
            {
                Tools.CheckForUpdates();
            }
            ForegroundManager.Subscribe();
            Application.Run(new MainWindow());
          
        }
    }
}
