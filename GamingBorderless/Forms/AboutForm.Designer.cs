﻿/*** File changed by DigitalNikki/Nicole M on Jan/21/2018 **/
namespace GamingBorderless.Forms
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.BG_Label = new System.Windows.Forms.Label();
            this.GitLabLabel = new System.Windows.Forms.Label();
            this.MaintainersLabel = new System.Windows.Forms.Label();
            this.ContributersLabel = new System.Windows.Forms.Label();
            this.GitLab_Globe = new System.Windows.Forms.PictureBox();
            this.CopyrightLabel = new System.Windows.Forms.Label();
            this.RandomGamers_Label = new System.Windows.Forms.Label();
            this.RandomGamers_Globe = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.GitLab_Globe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RandomGamers_Globe)).BeginInit();
            this.SuspendLayout();
            // 
            // BG_Label
            // 
            resources.ApplyResources(this.BG_Label, "BG_Label");
            this.BG_Label.Name = "BG_Label";
            this.BG_Label.Click += new System.EventHandler(this.BG_Label_Click);
            // 
            // GitLabLabel
            // 
            resources.ApplyResources(this.GitLabLabel, "GitLabLabel");
            this.GitLabLabel.Name = "GitLabLabel";
            // 
            // MaintainersLabel
            // 
            resources.ApplyResources(this.MaintainersLabel, "MaintainersLabel");
            this.MaintainersLabel.Name = "MaintainersLabel";
            // 
            // ContributersLabel
            // 
            resources.ApplyResources(this.ContributersLabel, "ContributersLabel");
            this.ContributersLabel.Name = "ContributersLabel";
            // 
            // GitLab_Globe
            // 
            this.GitLab_Globe.Image = global::GamingBorderless.Properties.Resources.globe_green;
            resources.ApplyResources(this.GitLab_Globe, "GitLab_Globe");
            this.GitLab_Globe.Name = "GitLab_Globe";
            this.GitLab_Globe.TabStop = false;
            this.GitLab_Globe.Click += new System.EventHandler(this.GitLab_Link);
            // 
            // CopyrightLabel
            // 
            resources.ApplyResources(this.CopyrightLabel, "CopyrightLabel");
            this.CopyrightLabel.Name = "CopyrightLabel";
            // 
            // RandomGamers_Label
            // 
            resources.ApplyResources(this.RandomGamers_Label, "RandomGamers_Label");
            this.RandomGamers_Label.Name = "RandomGamers_Label";
            // 
            // RandomGamers_Globe
            // 
            this.RandomGamers_Globe.Image = global::GamingBorderless.Properties.Resources.globe_green;
            resources.ApplyResources(this.RandomGamers_Globe, "RandomGamers_Globe");
            this.RandomGamers_Globe.Name = "RandomGamers_Globe";
            this.RandomGamers_Globe.TabStop = false;
            this.RandomGamers_Globe.Click += new System.EventHandler(this.RG_Link);
            // 
            // AboutForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RandomGamers_Globe);
            this.Controls.Add(this.RandomGamers_Label);
            this.Controls.Add(this.GitLab_Globe);
            this.Controls.Add(this.ContributersLabel);
            this.Controls.Add(this.MaintainersLabel);
            this.Controls.Add(this.GitLabLabel);
            this.Controls.Add(this.BG_Label);
            this.Controls.Add(this.CopyrightLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.Load += new System.EventHandler(this.AboutFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.GitLab_Globe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RandomGamers_Globe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BG_Label;
		private System.Windows.Forms.Label CopyrightLabel;
        private System.Windows.Forms.Label GitLabLabel;
        private System.Windows.Forms.Label MaintainersLabel;
        private System.Windows.Forms.Label ContributersLabel;
        private System.Windows.Forms.PictureBox GitLab_Globe;
        private System.Windows.Forms.Label RandomGamers_Label;
        private System.Windows.Forms.PictureBox RandomGamers_Globe;
    }
}