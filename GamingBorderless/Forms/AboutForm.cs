/*** File changed by DigitalNikki/Nicole M on Jan/21/2018 **/
using System;
using System.Reflection;
using System.Windows.Forms;
using GamingBorderless.Logic.System;

namespace GamingBorderless.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutFormLoad(object sender, EventArgs e)
        {
            // removed .Version.ToString(2) in favor of just .ToString() here so we can see the build number now
            BG_Label.Text = "Gaming Borderless " + Assembly.GetExecutingAssembly().GetName().Version;
            CopyrightLabel.Text = "Copyright © 2018-" + DateTime.Now.Year + " Nicole Monson";
        }

        private void GitLab_Link(object sender, EventArgs e)
        {
            Tools.GotoSite("https://gitlab.com/DigitalNikki/GamingBorderless");
        }

        private void RG_Link(object sender, EventArgs e)
        {
            Tools.GotoSite("https://randomgamers.org/?page_id=241");
        }

        //#endregion

        #region Contributers

        #endregion

        private void BG_Label_Click(object sender, EventArgs e)
        {

        }
    }
}