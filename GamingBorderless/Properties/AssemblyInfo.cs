﻿// File changed by DigitalNikki/Nicole M on Jan/21/2018
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Gaming Borderless")]
[assembly: AssemblyDescription("Play your favorite games in a borderless window")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RandomGamers")]
[assembly: AssemblyProduct("Gaming Borderless")]
[assembly: AssemblyCopyright("Copyright © 2017-2021 Nicole Monson")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("a4ec5be3-e7a6-4d82-825a-b697248925dc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("9.5.6.3")]
[assembly: AssemblyFileVersion("9.5.6.3")]
[assembly: NeutralResourcesLanguage("en-US")]