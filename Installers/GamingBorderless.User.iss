;; File changed by aDigitalPhantom/Nicole M on Jan/21/2018
[Setup]
#define MainProg "../GamingBorderless/bin/Release/User/GamingBorderless.exe"
#define Major
#define Minor
#define Rev
#define Build
#define Version ParseVersion(MainProg, Major, Minor, Rev, Build)
#define AppVersion Str(Major)+"."+Str(Minor)+(Rev > 0 ? "."+Str(Rev) : "")
AppName=Gaming Borderless
AppPublisher=Nicole Monson
AppCopyright=Copyright (C) 2017-2018 Nicole Monson
DefaultDirName={pf}\Gaming Borderless
DefaultGroupName=GamingBorderless
OutputDir=./
DisableReadyMemo=yes
DisableReadyPage=yes
SetupIconFile=../GamingBorderless/GamingBorderless.ico
Compression=lzma/ultra64
SolidCompression=yes
LicenseFile=../LICENSE
Uninstallable=yes
; Needed to modify %AppData%
PrivilegesRequired=admin
DisableProgramGroupPage=yes
DirExistsWarning=no

; Shown as installed version (Programs & Features) as well as product version ('Details' tab when right-clicking setup program and choosing 'Properties')
AppVersion={#AppVersion}
; Stored in the version info for the setup program itself ('Details' tab when right-clicking setup program and choosing 'Properties')
VersionInfoVersion={#Version}
; Other version info
OutputBaseFilename=GamingBorderless.User


; Shown in the setup program during install only
AppVerName=Gaming Borderless v{#AppVersion}

; Shown only in Programs & Features
AppContact=Gaming Borderless on RandomGamers.org
AppComments=Play your favorite games in a borderless window; no more time-consuming Alt-Tabs!
AppPublisherURL=https://randomgamers.org/showthread.php?tid=26
AppUpdatesURL=https://randomgamers.org/showthread.php?tid=26
UninstallDisplayName=Gaming Borderless
; 691 KB as initial install
UninstallDisplaySize=929008
UninstallDisplayIcon={app}\GamingBorderless.exe


[Messages]
BeveledLabel=Gaming Borderless {#AppVersion} Setup

[Languages]
Name: english; MessagesFile: compiler:Default.isl

[Files]
Source: ../GamingBorderless/bin/Release/User/GamingBorderless.exe; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/GamingBorderless.Logic.dll; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/CommandLine.dll; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/DotNetZip.dll; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/protobuf-net.dll; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/Languages.zip; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/protobuf-net.dll; DestDir: {app}; Flags: ignoreversion
Source: ../GamingBorderless/bin/Release/User/LICENSE.txt; DestName: License.txt; DestDir: {app}
Source: ../GamingBorderless/bin/Release/User/README.txt; DestName: ReadMe.txt; DestDir: {app}
Source: ./uninstall.ico; DestDir: {app}

[Tasks]
;Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons};

[Icons]
Name: {commondesktop}\Gaming Borderless; Filename: {app}\GamingBorderless.exe; WorkingDir: {app}; Tasks: desktopicon
Name: {group}\Gaming Borderless; Filename: {app}\GamingBorderless.exe; WorkingDir: {app}
Name: {group}\Uninstall Gaming Borderless; Filename: {uninstallexe}; IconFileName: {app}\uninstall.ico
Name: {group}\License Agreement; Filename: {app}\License.txt
Name: {group}\Read Me; Filename: {app}\Read Me.txt

[Run]
Description: Start Gaming Borderless; Filename: {app}\GamingBorderless.exe; Flags: nowait postinstall skipifsilent shellexec

[UninstallDelete]
Type: files; Name: {app}\License.txt
Type: files; Name: {app}\Read Me.txt
Type: files; Name: {app}\uninstall.ico
Type: files; Name: {app}\GamingBorderless.exe
Type: files; Name: {app}\GamingBorderless.Logic.dll
Type: files; Name: {app}\CommandLine.dll
Type: files; Name: {app}\CommandLine.xml
Type: files; Name: {app}\DotNetZip.dll
Type: files; Name: {app}\protobuf-net.dll
Type: files; Name: {app}\Languages.zip
Type: files; Name: {app}\protobuf-net.dll

[Code]
procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then begin
    if MsgBox('Do you want to delete your Gaming Borderless settings and preferences as well?', mbConfirmation, MB_YESNO) = IDYES 
    then begin
      DelTree(ExpandConstant('{app}'), True, True, True)
    end;
  end;
end;
